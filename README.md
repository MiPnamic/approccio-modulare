# Approccio Modulare

Questo repository è strettamente collegato alla newsletter omonima disponibile a questo link:
https://orlandin.substack.com/

## Descrizione
Approccio Modulare è una contenuto pregno di Pragmatismo, Programmazione e derivati.

Questo repository è a supporto di tutto ciò che viene raccontato all'interno della newsletter.

## Supporto
Per qualsiasi errore o segnalazione sentitevi liberi di aprire una Issue qui su Gitlab.

## Autore
Questa newsletter, al momento, è scritta, pensata e redatta da [Marco Orlandin](https://orlandin.it)

## Licenza
Questo repository adotta la Licenza LGPLv3.
